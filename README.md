# Zhivtone Web

Web-клиент для Zhivtone

## Конфигурация

Конфигурация web-клиента находится в TOML-файле `config.toml` в рабочеЙ
директории проекта.

Пример конфигурации:

```toml
# Данные об API-сервере
[api]
domain                     = "api.local.host"
port                       = 443
# использовать TLS-шифрование при подключениии
use_tls                    = true
# разрещать самоподписанные сертификаты
allow_self_signed_api_cert = true

# Настройки сервера Web-клиента
[server]
domain                     = "localhost"
port                       = 8080
# использовать TLS-шифрование
use_tls                    = true
# путь до сертификата 
cert_file = "./apiserv.crt"
# путь до ключа
key_file  = "./apiserv.key"

# Настройки ресурсов
[assets]
# путь до директории с ресурсами
resources_path             = "../assets"

# Настройки работы
[settings]
# Закрыть регистрацию
close_registration         = true
```
