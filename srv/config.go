package main

import (
	"github.com/BurntSushi/toml"
	"io"
)

type Config struct {
	API struct {
		Domain                 string `toml:"domain"`
		Port                   uint16 `toml:"port"`
		UseTLS                 bool   `toml:"use_tls"`
		AllowSelfSignedAPICert bool   `toml:"allow_self_signed_api_cert"`
	} `toml:"api"`
	Server struct {
		Domain   string `toml:"domain"`
		Port     uint16 `toml:"port"`
		UseTLS   bool   `toml:"use_tls"`
		CertFile string `toml:"cert_file"`
		KeyFile  string `toml:"key_file"`
	} `toml:"server"`

	Assets struct {
		ResourcesPath string `toml:"resources_path"`
	} `toml:"assets"`

	Settings struct {
		CloseRegistration bool `toml:"close_registration"`
	} `toml:"settings"`
}

func (c *Config) InitFromTOML(stream io.Reader) error {
	_, err := toml.DecodeReader(stream, c)
	return err
}
