package main

import (
	// stdlib
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

// config
var conf Config

// logs
var (
	infoLog  *log.Logger = log.New(os.Stdout, " [  INF  ] ", log.Ldate|log.Ltime)
	errLog   *log.Logger = log.New(os.Stderr, " [ ERROR ] ", log.Ldate|log.Ltime|log.Llongfile)
	debugLog *log.Logger = log.New(io.Discard, " [ DEBUG ] ", log.Ldate|log.Ltime|log.Lshortfile)
)

// flag values
var (
	flagConfigPath string
	flagWriteDebug bool
	flagAPIusesTLS bool
)

// resources
var res *Resources = NewResources()

// API address and protocol
var (
	apiAdress      string
	protocolString = "http://"
)

func main() {
	defer infoLog.Println("HTTP-server stops serving...")

	mux := http.NewServeMux()

	mux.HandleFunc("/static/", staticHandler)
	mux.HandleFunc("/login", loginHandler)
	mux.HandleFunc("/register", registerHandler)
	mux.HandleFunc("/logout", logoutHandler)
	mux.HandleFunc("/main", mainHandler)
	mux.HandleFunc("/actions", actionsHandler)
	mux.HandleFunc("/add_action", addActionHandler)
	mux.HandleFunc("/", indexHandler)

	var err error

	if conf.Server.UseTLS {
		infoLog.Println("HTTPS-server starts serving...")
		err = http.ListenAndServeTLS(
			conf.Server.Domain+":"+strconv.Itoa(int(conf.Server.Port)),
			conf.Server.CertFile,
			conf.Server.KeyFile,
			mainMiddleware(mux),
		)
	} else {
		infoLog.Println("HTTP-server starts serving...")
		err = http.ListenAndServe(
			conf.Server.Domain+":"+strconv.Itoa(int(conf.Server.Port)),
			mainMiddleware(mux),
		)
	}

	if err != nil {
		errLog.Printf("Fatal error: %v\n", err)
	}

}
