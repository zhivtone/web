package main

import (
	// internal
	"codeberg.org/zhivtone/lib/execs"
)

type typesResponse struct {
	Result  []string `json:"Result"`
	Error   string   `json:"error"`
	Message string   `json:"message"`
}

type LoginTemplateStruct struct {
	RegistrationAvailable bool
	ErrorMessage          string
}

type RegisterTemplateStruct struct {
	ErrorMessage string
}

type MainTemplateStruct struct {
	Login        string
	LastBeat     string
	ErrorMessage string
}

type ActionsTemplateStruct struct {
	ErrorMessage string
	Actions      []_atsAction
}

type _atsAction struct {
	AfterStr   string
	Parameters map[string]execs.Param
	Executor   string
	Executed   bool
}

type AddActionTemplateStruct struct {
	ErrorMessage string
	Types        []string
}
