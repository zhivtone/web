package main

import (
	// zhivtone/lib
	"codeberg.org/zhivtone/lib/types"

	// stdlib
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	Page404 string = `<html><body><h1>404 Not Found (страница не найдена)</h1></body></html>`
	Page403 string = `<html><body><h1>403 Forbidden (доступ запрещён)</h1></body></html>`
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	if r.URL.Path != "/" && r.URL.Path != "" {
		w.Header().Add("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(404)
		w.Write([]byte(Page404))
		return
	}

	_, err := r.Cookie("user_authentication")
	if err != nil {
		w.Header().Add("Location", "/login")
	} else {
		w.Header().Add("Location", "/main")
	}
	w.WriteHeader(301)
}

func staticHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	name := strings.TrimSuffix(
		strings.TrimPrefix(
			strings.TrimPrefix(r.URL.Path, "/"),
			"static/",
		), "/",
	)
	data, is := res.Static.GetByName(name)
	if is {
		w.Header().Add("Content-Type", data.MIME)
		w.WriteHeader(200)
		w.Write(data.Data)
	} else {
		w.Header().Add("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(404)
		w.Write([]byte(Page404))
	}
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "text/html; charset=utf-8")

	var tStruct = LoginTemplateStruct{
		RegistrationAvailable: !conf.Settings.CloseRegistration,
	}

	_, err := r.Cookie("user_authentication")
	if err == nil {
		w.Header().Add("Location", "/main")
		w.WriteHeader(301)
		return
	}

	if r.Method == http.MethodGet {
		w.WriteHeader(200)
		err := res.Templates.Login.Execute(w, tStruct)
		if err != nil {
			errLog.Printf("error executing login template: %v\n", err)
		}
		return
	}

	if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
		w.WriteHeader(415)
		tStruct.ErrorMessage = "Внутренняя ошибка"
		err := res.Templates.Login.Execute(
			w,
			tStruct,
		)
		if err != nil {
			errLog.Printf("error executing login template: %v\n", err)
		}
		return
	}

	r.ParseForm()
	f := r.PostForm

	if f.Get("login") == "" || f.Get("password") == "" {
		w.WriteHeader(400)
		tStruct.ErrorMessage = "Логин или пароль пустой"
		err := res.Templates.Login.Execute(
			w,
			tStruct,
		)
		if err != nil {
			errLog.Printf("error executing login template: %v\n", err)
		}
		return
	}

	if !isLoginAllowed(f.Get("login")) {
		w.WriteHeader(400)
		tStruct.ErrorMessage = "Логин должен содержать только буквы " +
			"латинского алфавита, цифры, дефис и подчёркивание"
		err := res.Templates.Login.Execute(
			w,
			tStruct,
		)
		if err != nil {
			errLog.Printf("error executing login template: %v\n", err)
		}
		return
	}

	var (
		LoginDataBased = base64.StdEncoding.EncodeToString([]byte(f.Get("login") + ":" + f.Get("password")))
		AuthData       = "Basic " + LoginDataBased
	)

	rs, err := getUserByCreds(AuthData)

	if err != nil {
		errLog.Printf("Error accessing to API: %v\n", err)
		w.WriteHeader(500)
		tStruct.ErrorMessage = "Внутренняя ошибка"
		err := res.Templates.Login.Execute(
			w, tStruct,
		)
		if err != nil {
			errLog.Printf("error executing login template: %v\n", err)
		}
		return
	}

	if rs.Error != "" {
		if rs.Message == "wrong password" || rs.Message == "users with this login not found" {
			w.WriteHeader(400)
			tStruct.ErrorMessage = "Неправильный логин или пароль"
			err := res.Templates.Login.Execute(w, tStruct)
			if err != nil {
				errLog.Printf("error executing login template: %v\n", err)
			}
		} else {
			errLog.Printf("Error with get_user request: %s\n", rs.Error)
			w.WriteHeader(500)
			tStruct.ErrorMessage = "Внутренняя ошибка"
			err := res.Templates.Login.Execute(w, tStruct)
			if err != nil {
				errLog.Printf("error executing login template: %v\n", err)
			}
		}
		return
	}

	c := &http.Cookie{
		Name:     "user_authentication",
		Value:    AuthData,
		Secure:   true,
		SameSite: http.SameSiteStrictMode,
	}

	if f.Get("guest") != "1" {
		c.Expires = time.Now().Add(5 * 12 * 31 * 24 * time.Hour)
	}

	http.SetCookie(w, c)
	w.Header().Add("Location", "/main")
	w.WriteHeader(303)
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "text/html")

	if conf.Settings.CloseRegistration {
		w.Header().Add("Location", "/login")
		w.WriteHeader(303)
		return
	}

	_, err := r.Cookie("user_authentication")

	if err == nil {
		w.Header().Add("Location", "/main")
		w.WriteHeader(303)
		return
	}

	if r.Method == http.MethodGet {
		w.WriteHeader(200)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
		w.WriteHeader(415)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	r.ParseForm()
	f := r.PostForm

	if f.Get("login") == "" ||
		f.Get("password") == "" ||
		f.Get("password_repeat") == "" {
		w.WriteHeader(400)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Одно или несколько обязательных полей не заполнены",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	if f.Get("password") != f.Get("password_repeat") {
		w.WriteHeader(400)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Пароли не совпадают",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	if !isLoginAllowed(f.Get("login")) {
		w.WriteHeader(400)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Логин может содержать только символы латиницы, цифры, дефисы (-) и нижние подчёркивания (_)",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	if len(f.Get("password")) < 8 {
		w.WriteHeader(400)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Пароль должен содержать минимум 8 символов",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	var regData = types.RegisterRequest{
		Login:       f.Get("login"),
		PasswordB64: base64.StdEncoding.EncodeToString([]byte(f.Get("password"))),
	}

	regDataEncoded, err := json.Marshal(regData)

	if err != nil {
		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	req, err := http.NewRequest(
		http.MethodPost,
		protocolString+apiAdress+"/register",
		bytes.NewReader(regDataEncoded),
	)

	if err != nil {
		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	req.Header.Add("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	var respStruct types.RegisterResponse

	err = json.Unmarshal(body, &respStruct)
	if err != nil {
		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	if respStruct.Error != "" {
		if respStruct.Message == "login is already taken" {
			w.WriteHeader(409)
			err = res.Templates.Register.Execute(
				w,
				RegisterTemplateStruct{
					ErrorMessage: "Этот логин уже занят",
				},
			)
			if err != nil {
				errLog.Printf("Error executing register template: %v\n", err)
			}
			return
		}

		w.WriteHeader(500)
		err = res.Templates.Register.Execute(
			w,
			RegisterTemplateStruct{
				ErrorMessage: "Внутренняя ошибка",
			},
		)
		if err != nil {
			errLog.Printf("Error executing register template: %v\n", err)
		}
		return
	}

	var LoginDataBased = base64.StdEncoding.EncodeToString([]byte(f.Get("login") + ":" + f.Get("password")))

	c := &http.Cookie{
		Name:     "user_authentication",
		Value:    "Basic " + LoginDataBased,
		Secure:   true,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, c)
	w.Header().Add("Location", "/main")
	w.WriteHeader(303)
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	if r.Method == http.MethodPost {
		c := &http.Cookie{
			Name:  "user_authentication",
			Value: "",

			MaxAge: -1,
		}
		http.SetCookie(w, c)
	}

	w.Header().Add("Location", "/login")
	w.WriteHeader(303)

}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "text/html")

	authC, err := r.Cookie("user_authentication")
	if err != nil {
		w.Header().Add("Location", "/login")
		w.WriteHeader(303)
		return
	}

	if r.Method == http.MethodPost {
		req, err := http.NewRequest(
			http.MethodGet,
			protocolString+apiAdress+"/alive",
			nil,
		)

		if err != nil {
			errLog.Printf("Error creating request: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.Main.Execute(
				w,
				MainTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		req.Header.Add("Authorization", authC.Value)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			errLog.Printf("Error doing request: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.Main.Execute(
				w,
				MainTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		defer resp.Body.Close()
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			errLog.Printf("Error reading body: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.Main.Execute(
				w,
				MainTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}
		var bu = make(map[string]string, 0)
		err = json.Unmarshal(b, &bu)
		if err != nil {
			errLog.Printf("Error unmarshalling json: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.Main.Execute(
				w,
				MainTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}
		if x, ok := bu["error"]; ok && x != "" {
			if x == "Unauthorized" {
				w.Header().Add("Location", "/logout")
				w.WriteHeader(303)
			} else {
				errLog.Printf("Error accessing to api: %s\n", bu["msg"])
				w.WriteHeader(500)
				err = res.Templates.Main.Execute(
					w,
					MainTemplateStruct{
						ErrorMessage: "Внутренняя ошибка",
					},
				)
				if err != nil {
					errLog.Printf("Error executing template: %v\n", err)
				}
			}
			return
		}
	}

	rs, err := getUserByCreds(authC.Value)

	if err != nil || rs.Error != "" {
		w.Header().Add("Location", "/logout")
		w.WriteHeader(303)
		return
	}

	w.WriteHeader(200)

	var respStruct = MainTemplateStruct{
		Login: rs.Login,
	}

	if !rs.LastBeat.IsZero() {
		respStruct.LastBeat = rs.LastBeat.Format("02.01.2006 15:04")
	}

	err = res.Templates.Main.Execute(
		w,
		respStruct,
	)
	if err != nil {
		errLog.Printf("error executing template: %v\n", err)
	}

}

func actionsHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "text/html")

	authC, err := r.Cookie("user_authentication")
	if err != nil {
		w.Header().Add("Location", "/login")
		w.WriteHeader(303)
		return
	}

	rs, err := getUserByCreds(authC.Value)

	if err != nil || rs.Error != "" {
		w.Header().Add("Location", "/logout")
		w.WriteHeader(303)
		return
	}

	var ats = ActionsTemplateStruct{}

	if rs.Settings.Actions != nil {
		ats.Actions = make([]_atsAction, 0)
		for _, v := range rs.Settings.Actions {
			ats.Actions = append(ats.Actions, _atsAction{
				AfterStr:   strconv.Itoa(int(time.Duration(v.After).Hours())) + "ч",
				Parameters: v.Parameters,
				Executor:   string(v.Executor),
				Executed:   v.Executed,
			})
		}
	}

	w.WriteHeader(200)
	err = res.Templates.Actions.Execute(w, ats)
	if err != nil {
		errLog.Printf("Error executing template: %v\n", err)
	}
}

func addActionHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	authC, err := r.Cookie("user_authentication")
	if err != nil {
		w.Header().Add("Location", "/login")
		w.WriteHeader(303)
		return
	}

	rs, err := getUserByCreds(authC.Value)
	_ = rs
	if err != nil {
		w.Header().Add("Location", "/logout")
		w.WriteHeader(303)
		return
	}

	w.Header().Add("Content-Type", "text/html")

	var types []string

	{
		req, err := http.NewRequest(
			http.MethodPost,
			protocolString+apiAdress+"/types",
			nil,
		)

		if err != nil {
			errLog.Printf("Error creating request: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.AddAction.Execute(
				w,
				AddActionTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			errLog.Printf("Error doing request: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.AddAction.Execute(
				w,
				AddActionTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		defer resp.Body.Close()
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			errLog.Printf("Error reading body: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.AddAction.Execute(
				w,
				AddActionTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}
		var ts = typesResponse{}
		err = json.Unmarshal(b, &ts)
		if err != nil {
			errLog.Printf("Error unmarshalling json: %v\n", err)
			w.WriteHeader(500)
			err = res.Templates.AddAction.Execute(
				w,
				AddActionTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		if ts.Error != "" {
			errLog.Printf("Error accessing to api: %s\n", ts.Message)
			w.WriteHeader(500)
			err = res.Templates.AddAction.Execute(
				w,
				AddActionTemplateStruct{
					ErrorMessage: "Внутренняя ошибка",
				},
			)
			if err != nil {
				errLog.Printf("Error executing template: %v\n", err)
			}
			return
		}

		types = ts.Result
	}

	if !r.URL.Query().Has("type") {
		err = res.Templates.AddAction.Execute(w,
			AddActionTemplateStruct{
				Types: types,
			},
		)
		if err != nil {
			errLog.Printf("Error executing template: %v\n", err)
		}
		return
	}

	{
		var isInArr bool

		for _, el := range types {
			if el == r.URL.Query().Get("type") {
				isInArr = true
				break
			}
		}

		if !isInArr {
			w.Header().Add("Location", "/add_action")
			w.WriteHeader(303)
			return
		}
	}
}

func mainMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			debugLog.Printf("%s\n", r.URL.Path)
			handler.ServeHTTP(w, r)
		},
	)
}
