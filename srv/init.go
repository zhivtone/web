package main

import (
	// stdlib
	"crypto/tls"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

func init() {
	// flag initializing
	flag.StringVar(&flagConfigPath, "config", "./config.toml", "path to config file")
	flag.BoolVar(&flagWriteDebug, "debug", false, "directs debug log to output (stderr)")
	flag.StringVar(&apiAdress, "api-adress", "localhost:8181", "domain:port of API server")
	flag.BoolVar(&flagAPIusesTLS, "api-tls", false, "use TLS (https) to connect to API")
	flag.Parse()

	// debug log directing
	if flagWriteDebug {
		debugLog.SetOutput(os.Stderr)
	}

	// config initializing
	{
		file, err := os.Open(flagConfigPath)
		if err != nil {
			errLog.Printf("Error reading config: %v\n", err)
			os.Exit(1)
		}
		err = conf.InitFromTOML(file)
		if err != nil {
			errLog.Printf("Error parsing TOML: %v\n", err)
			os.Exit(1)
		}
	}

	// resources initializing
	{
		err := res.Load(conf.Assets.ResourcesPath)
		if err != nil {
			errLog.Printf("Error loading resources: %v\n", err)
			os.Exit(1)
		}
	}

	// API address finding, sending and pinging
	{
		if conf.API.AllowSelfSignedAPICert {
			http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
		}

		if conf.API.UseTLS || flagAPIusesTLS {
			protocolString = "https://"
		}

		var set bool
		for _, ad := range []string{
			conf.API.Domain + ":" + strconv.Itoa(int(conf.API.Port)),
			apiAdress,
		} {
			_, err := http.Get(protocolString + ad + "/ping")
			if err == nil {
				apiAdress = ad
				set = true
				break
			}
		}

		if !set {
			errLog.Print("Error accessing to API server\n")
			os.Exit(1)
		}
	}

	// Get server settings
	{
		resp, err := http.Get(protocolString + apiAdress + "/settings")
		if err == nil {
			defer resp.Body.Close()
			b, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				errLog.Print("Error decoding API answer\n")
				os.Exit(1)
			}
			var respStruct _srvSettings
			err = json.Unmarshal(b, &respStruct)
			if err != nil {
				errLog.Print("Error decoding API answer\n")
				os.Exit(1)
			}

			conf.Settings.CloseRegistration =
				conf.Settings.CloseRegistration ||
				respStruct.Result.CloseRegistration

		} else {
			errLog.Print("Error accessing to API/settings\n")
		}
	}
}
