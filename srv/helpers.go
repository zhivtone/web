package main

import (
	// internal
	"codeberg.org/zhivtone/lib/types"

	// stdlib
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect" // oh...
)

// [a-zA-Z0-9\-_]
var allowedLoginSymbols = [...]rune{
	'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u',
	'v', 'w', 'x', 'y', 'z',
	'A', 'B', 'C', 'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U',
	'V', 'W', 'X', 'Y', 'Z',
	'0', '1', '2', '3', '4', '5', '6',
	'7', '8', '9', '_', '-',
}

func isLoginAllowed(login string) bool {
LOGIN_FOR:
	for _, s := range login {
		for _, a := range allowedLoginSymbols {
			if s == a {
				continue LOGIN_FOR
			}
		}
		return false
	}
	return true
}

func getUserByCreds(creds string) (types.GetUserResponse, error) {
	var respStruct types.GetUserResponse

	req, err := http.NewRequest(
		http.MethodGet,
		protocolString+apiAdress+"/get_user",
		nil,
	)

	if err != nil {
		return respStruct, err
	}

	req.Header.Add("Authorization", creds)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return respStruct, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return respStruct, err
	}

	err = json.Unmarshal(body, &respStruct)
	if err != nil {
		return respStruct, err
	}

	return respStruct, nil
}

func isIterable(value interface{}) bool {
	k := reflect.TypeOf(value).Kind()
	return k == reflect.Slice || k == reflect.Array || k == reflect.Map
}

type _srvSettings struct {
	Result struct {
		CloseRegistration bool `json:"close_registration"`
	} `json:"result"`
}
