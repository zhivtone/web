package main

import (
	"html/template"
	"io/ioutil"
	"os"
)

type Resources struct {
	Static    *StaticResources
	Templates *TemplResources
}

func NewResources() *Resources {
	var r = &Resources{}
	r.Templates = NewTemplResources()
	r.Static = NewStaticResources()
	return r
}

func (r *Resources) Load(path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}
	var (
		sepDir       = path + string(os.PathSeparator)
		templSubdir  = sepDir + "templ"
		staticSubdir = sepDir + "static"
	)

	_, err = os.Stat(templSubdir)
	if err != nil {
		return err
	}
	_, err = os.Stat(staticSubdir)
	if err != nil {
		return err
	}

	err = r.Templates.Load(templSubdir)
	if err != nil {
		return err
	}

	err = r.Static.Load(staticSubdir)

	return err
}

// ================
// Static resources
// ================

type StaticFile struct {
	Data []byte
	MIME string
}

type StaticResources map[string]StaticFile

func NewStaticResources() *StaticResources {
	var sr = &StaticResources{
		"index.css": StaticFile{
			MIME: "text/css",
		},
		"logo.png": StaticFile{
			MIME: "image/png",
		},
		"logo.svg": StaticFile{
			MIME: "image/svg+xml",
		},
		"logo_small.png": StaticFile{
			MIME: "image/png",
		},
		"logo_small.svg": StaticFile{
			MIME: "image/svg+xml",
		},
	}
	return sr
}

func (s *StaticResources) Load(dir string) error {
	for filename := range *s {
		file, err := os.Open(dir + string(os.PathSeparator) + filename)
		if err != nil {
			return err
		}
		defer file.Close()
		b, err := ioutil.ReadAll(file)
		if err != nil {
			return err
		}
		el := (*s)[filename]
		el.Data = b
		(*s)[filename] = el
	}

	return nil
}

func (s *StaticResources) GetByName(name string) (StaticFile, bool) {
	file, is := (*s)[name]
	return file, is
}

// ==================
// Template Resources
// ==================

type TemplResources struct {
	Login     *template.Template
	Register  *template.Template
	Main      *template.Template
	Actions   *template.Template
	AddAction *template.Template
}

func NewTemplResources() *TemplResources {
	var tr = &TemplResources{}
	tr.Login = template.New("login.gohtml")
	tr.Register = template.New("register.gohtml")
	tr.Main = template.New("main.gohtml")
	tr.Actions = template.New("actions.gohtml").Funcs(
		template.FuncMap{
			"iterable": isIterable,
		},
	)
	tr.AddAction = template.New("add_action.gohtml").Funcs(
		template.FuncMap{
			"iterable": isIterable,
		},
	)
	return tr
}

func (t *TemplResources) Load(dir string) error {
	var prefix string = dir + string(os.PathSeparator)
	var templBase = []string{
		prefix + "header" + ".gohtml",
		prefix + "title" + ".gohtml",
	}

	var templMap = map[string]*template.Template{
		"login":      t.Login,
		"register":   t.Register,
		"main":       t.Main,
		"actions":    t.Actions,
		"add_action": t.AddAction,
	}

	for name, to := range templMap {
		_, err := to.ParseFiles(
			append(
				templBase,
				prefix+name+".gohtml",
			)...,
		)
		if err != nil {
			return err
		}
	}

	return nil
}
