module codeberg.org/zhivtone/web

go 1.20

require (
	codeberg.org/zhivtone/lib v0.0.0-20230606192853-178923a8c504
	github.com/BurntSushi/toml v1.3.0
)

require go.mongodb.org/mongo-driver v1.11.6 // indirect
