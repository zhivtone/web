CC=gcc
CXX=g++
GOC=go
GOFLAGS=
GO_LDFLAGS=
GOTAGS=
BUILDDIR=build
BINEXT=

all: server

server:
	$(GOC) build -ldflags="$(GO_LDFLAGS)" -tags="$(GOTAGS)" -o="$(BUILDDIR)/srv$(BINEXT)" $(GOFLAGS) ./srv

clean:
	rm -rf $(BUILDDIR)/srv$(BINEXT)
